package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ImageFileFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageSerializer;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;
import java.nio.file.Files;
import java.text.MessageFormat;

/**
 * Implémentation Base64 de sérialiseur d'image, basée sur des flux.
 * TODO
 */
public class ImageSerializerBase64DatabaseBetaImpl
        extends AbstractStreamingImageSerializer<File, ImageFileFrame> {

    public DataOutputStream getDos() {
        return dos;
    }

    private DataOutputStream dos;

    /**
     * {@inheritDoc}
     *
     * @param source
     * @return
     * @throws FileNotFoundException
     */
    @Override
    public InputStream getSourceInputStream(File source) throws FileNotFoundException {
        return new FileInputStream(source);
    }

    /**
     * Permet d'initialiser un flux d'écriture de donées avec remise du compteur à 0
     *
     * @param media
     */
    private void initDataStream(ImageFileFrame media) throws IOException {
        if (dos != null){
            dos.close();
        }
        this.dos = new DataOutputStream(media.getEncodedImageOutput());
    }
    /**
     * {@inheritDoc}
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(ImageFileFrame media) throws IOException {

        if (dos == null){
            initDataStream(media);
        }
        return new Base64OutputStream(dos,true,0,null);
    }


    @Override
    public final void serialize(File source, ImageFileFrame media) throws IOException {
        long numberOfLines = Files.lines(media.getChannel().toPath()).count();
        long size = Files.size(source.toPath());
        try(OutputStream os = media.getEncodedImageOutput()) {
            Writer writer = new PrintWriter(os);
            writer.write(MessageFormat.format("{0,number,#};{1,number,#};", numberOfLines + 1, size));
            writer.flush();
            try(OutputStream eos = getSerializingStream(media)) {
                getSourceInputStream(source).transferTo(eos);
                writer.write(MessageFormat.format(";{0,number,#}\n", dos.size()));
            }
            writer.flush();
            // On repositionne les compteurs de lecture à 0
            initDataStream(media);
        }
    }

}
