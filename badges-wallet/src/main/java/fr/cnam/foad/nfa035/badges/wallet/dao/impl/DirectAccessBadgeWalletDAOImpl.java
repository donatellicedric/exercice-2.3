package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.List;

public class DirectAccessBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {
   private final File walletDatabase;

   private static final Logger LOG = LogManager.getLogger(DirectAccessBadgeWalletDAOImpl.class);

   private ImageStreamingDeserializer<ResumableImageFrameMedia> deserializer;

   public DirectAccessBadgeWalletDAOImpl(String dbPath) throws IOException {
      this.walletDatabase = new File(dbPath);
      this.deserializer = deserializer;
   }

   @Override
   public void addBadge(File image) throws IOException {
      try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
         // A COMPLéTER
      } catch (Exception e) {
          throw new RuntimeException(e);
      }
   }

   @Override
   public void getBadge(OutputStream imageStream) throws FileNotFoundException {
      try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
         new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
      } catch (Exception e) {
          throw new RuntimeException(e);
      }
   }

   @Override
   public List<DigitalBadgeMetadata> getWalletMetadata() throws IOException {
      try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
         // A COMPLéTER

      } catch (Exception e) {
         throw new RuntimeException(e);
      }
      return (List<DigitalBadgeMetadata>) walletDatabase;
   }


       @Override
   public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException {
      List<DigitalBadgeMetadata> metas = this.getWalletMetadata();
      try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rws"))) {
         // A COMPLéTER
      } catch (Exception e) {
          throw new RuntimeException(e);
      }
   }
}
