package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;
import java.util.Objects;

public class DigitalBadgeMetadata {

    public int badgeld;

    public long ImageSize;

    public long walletPosition;

    /**
     * constructeur
     * @param bad
     * @param Image
     * @param walletPos
     */
    public  DigitalBadgeMetadata(int bad, long Image, long walletPos) {
        badgeld = bad;
        ImageSize = Image;
        walletPosition = walletPos;

    }

    /**
     * test d'égalité des paramètres
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitalBadgeMetadata that = (DigitalBadgeMetadata) o;
        return badgeld == that.badgeld && ImageSize == that.ImageSize && walletPosition == that.walletPosition;
    }

    /**
     *
     * @return badgeld
     */
    public int getBadgeld() {
        return badgeld;
    }

    /**
     *
     * @return ImageSize
     */
    public long getImageSize() {
        return ImageSize;
    }

    /**
     *
     * @return walletPosition
     */
    public long getWalletPosition() {
        return walletPosition;
    }

    /**
     *
     * @return un objet hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(badgeld, ImageSize, walletPosition);
    }

    /**
     * change le numéro du badge
     * @param badgeld
     */
    public void setBadgeld(int badgeld) {
        this.badgeld = badgeld;
    }

    /**
     * change la taille de l'image
     * @param imageSize
     */
    public void setImageSize(long imageSize) {
        ImageSize = imageSize;
    }

    /**
     * change la position du wallet
     * @param walletPosition
     */
    public void setWalletPosition(long walletPosition) {
        this.walletPosition = walletPosition;
    }

    /**
     *
     * @return string badgeld,ImageSize et walletPosition
     */
    @Override
    public String toString() {
        return "DigitalBadgeMetadata{" +
                "badgeld=" + badgeld +
                ", ImageSize=" + ImageSize +
                ", walletPosition=" + walletPosition +
                '}';
    }
}
