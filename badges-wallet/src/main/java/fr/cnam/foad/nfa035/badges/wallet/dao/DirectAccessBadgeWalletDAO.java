package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public interface DirectAccessBadgeWalletDAO extends BadgeWalletDAO {
    List<DigitalBadgeMetadata> getWalletMetadata() throws IOException;
    void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException;
}
