package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class WalletDeserializerDirectAccessimpl implements DirectAccessDatabaseDeserializer {

    public List<DigitalBadgeMetadata> metas;

    private static final Logger LOG = LogManager.getLogger(WalletDeserializerDirectAccessimpl.class);

    public OutputStream sourceOutputStream;

    /**
     * constructeur
     * @param source
     * @param m
     */
    public WalletDeserializerDirectAccessimpl(OutputStream source,List<DigitalBadgeMetadata> m){
        sourceOutputStream = source;
        metas = m;

    }

    /**
     * deserialise
     * @param media
     * @param meta
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException {
        long pos = meta.getWalletPosition();
        media.getChannel().seek(pos);
        String[] data = media.getEncodedImageReader(false).readLine().split(";");
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(data[3]).transferTo(os);
        }
    }

    /**
     * deserialise
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media) throws IOException {

    }

    /**
     *
     * @param data
     * @return null
     * @param <K>
     * @throws IOException
     */
    @Override
    public <K extends InputStream> K getDeserializingStream(String data) throws IOException {
        return null;
    }

    /**
     * renvoi le paramètre sourceOutputstream
     * @return sourceOutputStream
     */
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    /**
     * change le parametre sourceOutputStream
     * @param sourceOutputStream
     */
    public void setSourceOutputStream(OutputStream sourceOutputStream) {
        this.sourceOutputStream = sourceOutputStream;
    }
}
