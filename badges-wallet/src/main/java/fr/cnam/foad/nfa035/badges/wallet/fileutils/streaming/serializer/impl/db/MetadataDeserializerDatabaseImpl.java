package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.MetadataDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.List;

public class MetadataDeserializerDatabaseImpl implements MetadataDeserializer {

    private static final Logger Log = LogManager.getLogger(MetadataDeserializerDatabaseImpl.class);

    public OutputStream SourceOutputStream;
    
    @Override
    public List<DigitalBadgeMetadata> deserialize(WalletFrameMedia media) throws IOException {
        String[] data = media.getEncodedImageReader(false).readLine().split(";");

        return (List<DigitalBadgeMetadata>) data;


    }

        // A COMPLéTER


    /**
     *
     * @param file
     * @return string builder
     * @throws IOException
     */
    public static String readLastLine(RandomAccessFile file) throws IOException {
        StringBuilder builder = new StringBuilder();
        long length = file.length();
        for(long seek = length; seek >= 0; --seek){
            file.seek(seek);
            char c = (char)file.read();
            if(c != '\n' || seek == 1)
            {
                builder.append(c);
            }
            else{
                builder = builder.reverse();
                break;
            }
        }
        return builder.toString();
    }

}
