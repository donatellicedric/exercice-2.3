package fr.cnam.foad.nfa035.badges.wallet.model;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.Objects;

public class DigitalBadge {

    public File badge;

    public DigitalBadgeMetadata Metadata;

    /**
     * Constructeur
     * @param DigBadge
     * @param data
     */
    public DigitalBadge(File DigBadge, DigitalBadgeMetadata data){
        badge = DigBadge;
        Metadata = data;
    }

    /**
     *
     * @param o
     * @return boolean test d'égalité des paramètres
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitalBadge that = (DigitalBadge) o;
        return Objects.equals(badge, that.badge) && Objects.equals(Metadata, that.Metadata);
    }

    /**
     *
     * @return paramètre badge
     */
    public File getBadge() {
        return badge;
    }

    /**
     *
     * @return paramètre métadata
     */
    public DigitalBadgeMetadata getMetadata() {
        return Metadata;
    }

    /**
     *
     * @return objet hascode de badge et Metadata
     */
    @Override
    public int hashCode() {
        return Objects.hash(badge, Metadata);
    }

    /**
     * change le paramètre de badge
     * @param badge
     */
    public void setBadge(File badge) {
        this.badge = badge;
    }

    /**
     * change le paramètre Metadata
     * @param metadata
     */
    public void setMetadata(DigitalBadgeMetadata metadata) {
        Metadata = metadata;
    }

    /**
     *
     * @return string badge et Metadata
     */
    @Override
    public String toString() {
        return "DigitalBadge{" +
                "badge=" + badge +
                ", Metadata=" + Metadata +
                '}';
    }
}
