package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.MultiBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.simpleaccess.ImageSerializerBase64Impl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class DirectAccessBadgeWalletDAOTest {

    private static final Logger LOG = LogManager.getLogger(DirectAccessBadgeWalletDAOTest.class);
    
    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.csv");
    
    @BeforeEach
    public void init() throws IOException {
        if (walletDatabase.exists()){
            walletDatabase.delete();
            walletDatabase.createNewFile();
        }
    }

    @Test
    public void testAddBadgeOnDatabase(){
    
    try {
    
        DirectAccessBadgeWalletDAO dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "wallet.csv");
        
        // 1er Badge
        File image = new File(RESOURCES_PATH + "petite_image.png");
        dao.addBadge(image);
        
        // 2ème Badge
        File image2 = new File(RESOURCES_PATH + "petite_image_2.png");
        dao.addBadge(image2);
        
        // 3ème Badge
        File image3 = new File(RESOURCES_PATH + "superman.jpg");
        dao.addBadge(image3);
        
        try (BufferedReader reader = new BufferedReader(new FileReader(walletDatabase))) {
        
           ImageSerializer serializer = new ImageSerializerBase64Impl();
        
           String serializedImage = reader.readLine();
           LOG.info("1ère ligne:\n{}", serializedImage);
           String encodedImage = (String) serializer.serialize(image);
           serializedImage = serializedImage.replaceAll("\n", "").replaceAll("\r", "");
           String[] data = serializedImage.split(";");
        
           String serializedImage2 = reader.readLine();
           LOG.info("2ème ligne:\n{}", serializedImage2);
           String encodedImage2 = (String) serializer.serialize(image2);
           serializedImage2 = serializedImage2.replaceAll("\n", "").replaceAll("\r", "");
           String[] data2 = serializedImage2.split(";");
        
           String serializedImage3 = reader.readLine();
           LOG.info("3ème ligne:\n{}", serializedImage3);
           String encodedImage3 = (String) serializer.serialize(image3);
           serializedImage3 = serializedImage3.replaceAll("\n", "").replaceAll("\r", "");
           String[] data3 = serializedImage3.split(";");
        
           // Assertions
        
           assertEquals(1, Integer.parseInt(data[0]));
           assertEquals(0, Integer.parseInt(data[1]));
           assertEquals(Files.size(image.toPath()), Long.parseLong(data[2]));
           assertEquals(encodedImage, data[3]);
        
           assertEquals(2, Integer.parseInt(data2[0]));
           assertEquals(753, Integer.parseInt(data2[1]));
           assertEquals(Files.size(image2.toPath()), Long.parseLong(data2[2]));
           assertEquals(encodedImage2, data2[3]);
        
           assertEquals(3, Integer.parseInt(data3[0]));
           assertEquals(1972, Integer.parseInt(data3[1]));
           assertEquals(Files.size(image3.toPath()), Long.parseLong(data3[2]));
           assertEquals(encodedImage3, data3[3]);
        }
        
        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }

    @Test
    public void testGetBadgeFromDatabase(){
        try {
            BadgeWalletDAO dao = new MultiBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.csv");
        
            File originImage1 = new File(RESOURCES_PATH + "petite_image.png");
            File originImage2 = new File(RESOURCES_PATH + "petite_image_2.png");
            File originImage3 = new File(RESOURCES_PATH + "superman.jpg");
            File extractedImage1 = new File(RESOURCES_PATH + "petite_image_ext.png");
            File extractedImage2 = new File(RESOURCES_PATH + "petite_image_2_ext.png");
            File extractedImage3 = new File(RESOURCES_PATH + "superman_ext.jpg");
            
            OutputStream fileBadgeStream1 = new FileOutputStream(extractedImage1);
            dao.getBadge(fileBadgeStream1);
            OutputStream fileBadgeStream2 = new FileOutputStream(extractedImage2);
            dao.getBadge(fileBadgeStream2);
            OutputStream fileBadgeStream3 = new FileOutputStream(extractedImage3);
            dao.getBadge(fileBadgeStream3);
            
            assertArrayEquals(Files.readAllBytes(originImage1.toPath()), Files.readAllBytes(extractedImage1.toPath()));
            LOG.info("Badge 1 récupéré avec succès");
            assertArrayEquals(Files.readAllBytes(originImage2.toPath()), Files.readAllBytes(extractedImage2.toPath()));
            LOG.info("Badge 2 récupéré avec succès");
            assertArrayEquals(Files.readAllBytes(originImage3.toPath()), Files.readAllBytes(extractedImage3.toPath()));
            LOG.info("Badge 3 récupéré avec succès");
        
        } catch (Exception e) {
            LOG.error("Test en échec ! ", e);
            fail();
        }
    }

    @Test
    void testGetMetadata() throws IOException {
        DirectAccessBadgeWalletDAO dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
        List<DigitalBadgeMetadata> metaList = dao.getWalletMetadata();
        LOG.info("Et voici les Métadonnées: {}", dao.getWalletMetadata());
        
        assertEquals(3, metaList.size());
        assertEquals(metaList.get(0), new DigitalBadgeMetadata(1, 0,557));
        assertEquals(metaList.get(1), new DigitalBadgeMetadata(2,753, 906));
        assertEquals(metaList.get(2), new DigitalBadgeMetadata(3,1972, 35664));
    }

  @Test
  public void testGetBadgeFromDatabaseByMetadata(){
    try {
        BadgeWalletDAO dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");

        File originImage1 = new File(RESOURCES_PATH + "petite_image.png");
        File originImage2 = new File(RESOURCES_PATH + "petite_image_2.png");
        File originImage3 = new File(RESOURCES_PATH + "superman.jpg");
        File extractedImage1 = new File(RESOURCES_PATH + "petite_image_ext.png");
        File extractedImage2 = new File(RESOURCES_PATH + "petite_image_2_ext.png");
        File extractedImage3 = new File(RESOURCES_PATH + "superman_ext.jpg");
        
        OutputStream fileBadgeStream1 = new FileOutputStream(extractedImage1);
        ((DirectAccessBadgeWalletDAOImpl) dao).getBadgeFromMetadata(fileBadgeStream1, new DigitalBadgeMetadata(1, 0,557));
        OutputStream fileBadgeStream2 = new FileOutputStream(extractedImage2);
        ((DirectAccessBadgeWalletDAOImpl) dao).getBadgeFromMetadata(fileBadgeStream2, new DigitalBadgeMetadata(2,753, 906));
        OutputStream fileBadgeStream3 = new FileOutputStream(extractedImage3);
        ((DirectAccessBadgeWalletDAOImpl) dao).getBadgeFromMetadata(fileBadgeStream3, new DigitalBadgeMetadata(3,1972, 35664));
        
        assertArrayEquals(Files.readAllBytes(originImage1.toPath()), Files.readAllBytes(extractedImage1.toPath()));
        LOG.info("Badge 1 récupéré avec succès");
        assertArrayEquals(Files.readAllBytes(originImage2.toPath()), Files.readAllBytes(extractedImage2.toPath()));
        LOG.info("Badge 2 récupéré avec succès");
        assertArrayEquals(Files.readAllBytes(originImage3.toPath()), Files.readAllBytes(extractedImage3.toPath()));
        LOG.info("Badge 3 récupéré avec succès");

    } catch (Exception e) {
        LOG.error("Test en échec ! ", e);
        fail();
    }
  }
}
