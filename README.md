### Bienvenue dans ce cours CNAM en FOAD, NF035

Vous trouverez dans ce fichier **Readme** l'énoncé de chaque exercice noté dans le cadre du projet tutoré central à cet enseignement sur **JAVA: Bibliothèques et Patterns**.
_La solution de chaque exercice fera d'ailleurs l'objet d'un nouveau projet Git ici-même._

**Même si votre solution est bonne et recevable avec tous les points, il est par ailleurs préférable de repartir de la présente solution pour avancer dans ce projet tutoré sans risque, étant donné ce qui vous sera demandé en suite.**

Voici l'énoncé de l'exercice suivant, avec en introduction, un bref rappel des objectifs de la session d'exercices.
Il s'agit du 3ème exercice noté de la 2ème session (=>Bloc d'enseignement) de ce projet.

---

# Listes, Ensembles et Couches logicielles
 ++ Intégration d'une couche logicielle transverse: commons/utils

## Contexte
* Au programme de ce cours: Listes, Ensembles, Couches logicielles
## Objectifs
* Mises en application:
 - [x] (Exercice 1) Homogénéisation du projet en suivant le standard Maven (Exercice 1)
 - [x] Permettre au wallet de recevoir plusieurs badges, et de les restituer sous forme de liste
    - [x] (Exercice 2) Ecriture/Lecture séquentielle mono-ligne dans le fichier texte, avec métadonnées simples, ID, Taille.
    - [x] **(Exercice 3) Lecture Du fichier texte en accès direct: stockage des métadonnées trouvées dans une Liste + Lecture d'un badge correspondant aux méta-données**
 - [ ] (Exercice 4) Permettre au wallet de recevoir plusieurs badges, et de les restituer sous forme de liste
    - [ ] Lecture Du fichier texte en accès direct et stockage des métadonnées trouvées dans un Set, donc... 
    - [ ] Ajout d'une fonction de hachage dans les métadonnées 
    - [ ] Ajout d'une gestion d'exception lors de l'ajout de badge pour garantir l'unicité des badges.


## Consignes de l'Exercice 3

Abordons à présent l'exercice 3 de cette Séance, avec comme défi celui d'accéder à notre base en accès direct.
Pour cela nous devons introduire la notion de méta-données (et le concept implicite d'indexation) afin de permettre de localiser les enregistrements de la base et permettre enfin d'y accéder directement.

 - [ ] **Pour commencer,n'oubliez pas notre approche TDD, et sachez que le test unitaire doit être pensé avant toute autre forme de codage. Pour vous aider, ce dernier est déjà présent en fin d'énoncer, imprégnez-vous en** !

### Modèle

```plantuml
@startuml

title __MODEL's Class Diagram__\n

  namespace fr.cnam.foad.nfa035.badges.wallet.model {
    class fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge {
        - badge : File
        + DigitalBadge()
        + equals()
        + getBadge()
        + getMetadata()
        + hashCode()
        + setBadge()
        + setMetadata()
        + toString()
    }
  }


  namespace fr.cnam.foad.nfa035.badges.wallet.model {
    class fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata {
        - badgeId : int
        - imageSize : long
        - walletPosition : long
        + DigitalBadgeMetadata()
        + equals()
        + getBadgeId()
        + getImageSize()
        + getWalletPosition()
        + hashCode()
        + setBadgeId()
        + setImageSize()
        + setWalletPosition()
        + toString()
    }
  }


  fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge o-right- fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata : metadata

@enduml
```
- [ ] En vous aidant du diagramme ci-dessus, implémentez la couche Modèle de notre projet, avec notamment le badge représenté par a classe **DigitalBadge** et ses métadonnées représentées par la classe **DigitalBadgeMetadata**

### Logique d'abstraction du media

```plantuml
@startuml

title __Design de l'Abstraction du Media de sérialisation__\n

  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace media {
      namespace impl {
        class fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.WalletFrame {
            ~ numberOfLines : long
            {static} - LOG : Logger
            - fileOutputStream : FileOutputStream
            - reader : BufferedReader
            + WalletFrame()
            + getEncodedImageInput()
            + getEncodedImageOutput()
            + getEncodedImageReader()
            + getNumberOfLines()
            + incrementLines()
        }
      }
    }
  }

  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.WalletFrame .up.|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.WalletFrame .up.|> java.lang.AutoCloseable

  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace media {
      interface fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia {
          {abstract} + getNumberOfLines()
          {abstract} + incrementLines()
      }
    }
  }

@enduml

```

 - [ ] En vous aidant du diagramme ci-dessus, implémentez à présent la couche d'abstraction du média, en notant au passage l'utilisation de l'interface **Autocloseable**:
   - [ ] L'interface **WalletFrameMedia** impliquant que pour un media en accès direct, il faut être en mesure de mémoriser le nombre de lignes et de l'incrémenter, afin de l'ajouter aux métadonnées de chaque enregistrement.
   
```java
public interface WalletFrameMedia extends ResumableImageFrameMedia<RandomAccessFile> {
   long getNumberOfLines();
   void incrementLines();
}
```

- [ ] La classe **WalletFrame**, une implémentation d'ImageFrame pour un Fichier csv comme canal, qui permet notamment de gérer l'amorce du csv afin de le rendre exploitable par accès direct (RandomAccessFile).

```java
public class WalletFrame extends AbstractImageFrameMedia<RandomAccessFile> implements WalletFrameMedia, AutoCloseable {

    private BufferedReader encodedImageReader;
    private FileOutputStream encodedImageOutput = null;
    private long numberOfLines;
    
    public WalletFrame(RandomAccessFile walletDatabase) {
        super(walletDatabase);
    }
    
    @Override
    public OutputStream getEncodedImageOutput() throws IOException {
        if (encodedImageOutput == null){
            RandomAccessFile file = getChannel();
            this.encodedImageOutput = new FileOutputStream(file.getFD());
            long fileLength = file.length();

            if(fileLength == 0) {
                // Ecriture de l'amorce
                Writer writer = new PrintWriter(encodedImageOutput, true, StandardCharsets.UTF_8);
                writer.write(MessageFormat.format("{0,number,#};{1,number,#};", 1, file.getFilePointer()));
                writer.flush();
            }
            else{
                BufferedReader br = getEncodedImageReader(false);
                String lastLine = MetadataDeserializerDatabaseImpl.readLastLine(file);
                String[]data = lastLine.split(";");
                this.numberOfLines = Long.parseLong(data[0]) - 1;
                file.seek(fileLength);
            }
        }
        return encodedImageOutput;
    }
    
    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return new BufferedInputStream(new FileInputStream(getChannel().getFD()));
    }
    
    @Override
    public BufferedReader getEncodedImageReader(boolean resume) throws IOException {
        if (encodedImageReader == null || !resume){
            this.encodedImageReader = new BufferedReader(new InputStreamReader(new FileInputStream(getChannel().getFD()), StandardCharsets.UTF_8));
        }
        return this.encodedImageReader;
    }

    @Override
    public long getNumberOfLines() {
        return numberOfLines;
    }

    @Override
    public void incrementLines() {
        this.numberOfLines++;
    }
}
```

### DAO

```plantuml
@startuml

title __DAO's Class Diagram__\n

  namespace fr.cnam.foad.nfa035.badges.wallet.dao {
    namespace impl {
      class fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl {
          {static} - LOG : Logger
          - deserializer : ImageStreamingDeserializer<ResumableImageFrameMedia>
          - walletDatabase : File
          + DirectAccessBadgeWalletDAOImpl()
          + addBadge()
          + getBadge()
          + getBadgeFromMetadata()
          + getWalletMetadata()
      }
    }
  }

  namespace fr.cnam.foad.nfa035.badges.wallet.dao {
    interface fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO {
        {abstract} + getBadgeFromMetadata()
        {abstract} + getWalletMetadata()
    }
  }

  fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl .up.|> fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO

@enduml
```

- [ ] En vous aidant du diagramme ci-dessus, implémentez à présent la couche DAO:
   - [ ] L'interface **DirectAccessBadgeWalletDAO** définissant le comportement élémentaire d'un DAO destinée à la gestion de badges digitaux PAR ACCES DIRECT, donc nécessitant la prise en compte de métadonnées.

```java

public interface DirectAccessBadgeWalletDAO extends BadgeWalletDAO {
    List<DigitalBadgeMetadata> getWalletMetadata() throws IOException;
    void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException;
}
```

   - [ ] La classe **DirectAccessBadgeWalletDAOImpl**, implémentation de DAO simple pour lecture/écriture d'un badge dans un wallet à badges digitaux multiples et PAR ACCES DIRECT, donc prenant en compte les métadonnées de chaque badges.

```java
public class DirectAccessBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {
   private final File walletDatabase;

   public DirectAccessBadgeWalletDAOImpl(String dbPath) throws IOException {
      this.walletDatabase = new File(dbPath);
   }

   @Override
   public void addBadge(File image) throws IOException {
      try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
         // A COMPLéTER
      }
   }

   @Override
   public void getBadge(OutputStream imageStream) throws IOException {
      try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
         new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
      }
   }

   @Override
   public List<DigitalBadgeMetadata> getWalletMetadata() throws IOException {
      try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
         // A COMPLéTER
      }
   }

   @Override
   public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException {
      List<DigitalBadgeMetadata> metas = this.getWalletMetadata();
      try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rws"))) {
         // A COMPLéTER
      }
   }
}
```

   - [ ] Prendre connaissance des 3 méthodes incomplètes, qui devront être complétées en dernier lieu.
     - [ ] **addBadge()** afin de mettre à jour l'amorce à chaque ajout de badge au Wallet
     - [ ] **getWalletMetadata()** pour récupérer l'ensemble des métadonnées du Wallet par balayage en flux de toutes les lignes du fichier csv
     - [ ] **getBadgeFromMetadata()** pour récupérer un seule et unique Badge par accès direct.   

### Noyeau de sérialisation

```plantuml
@startuml
title __DB's Class Diagram__\n
  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      namespace impl.db {
        class fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl {
            {static} - LOG : Logger
            - sourceOutputStream : OutputStream
            + deserialize()
            {static} + readLastLine()
        }
      }
    }
  }
  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      namespace impl.db {
        class fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.WalletDeserializerDirectAccessImpl {
            ~ metas : List<DigitalBadgeMetadata>
            {static} - LOG : Logger
            - sourceOutputStream : OutputStream
            + WalletDeserializerDirectAccessImpl()
            + deserialize()
            + deserialize()
            + getDeserializingStream()
            + getSourceOutputStream()
            + setSourceOutputStream()
        }
      }
    }
  }
  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      namespace impl.db {
        class fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.WalletSerializerDirectAccessImpl {
            + getSerializingStream()
            + getSourceInputStream()
            + serialize()
        }
      }
    }
  }
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl .up.|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.MetadataDeserializer
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.WalletDeserializerDirectAccessImpl .up.|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer -up-|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DatabaseDeserializer
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DatabaseDeserializer -up-|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.BadgeDeserializer
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.WalletSerializerDirectAccessImpl -up-|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageSerializer
  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      abstract class fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageSerializer {
          + serialize()
      }
    }
  }
  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      interface fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.BadgeDeserializer {
          {abstract} + deserialize()
          {abstract} + getSourceOutputStream()
          {abstract} + setSourceOutputStream()
      }
    }
  }
  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      interface fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DatabaseDeserializer {
          {abstract} + getDeserializingStream()
      }
    }
  }
  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      interface fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer {
          {abstract} + deserialize()
          {abstract} + getSourceOutputStream()
          {abstract} + setSourceOutputStream()
      }
    }
  }
  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      interface fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer {
          {abstract} + getSerializingStream()
          {abstract} + getSourceInputStream()
          {abstract} + serialize()
      }
    }
  }
  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      interface fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.MetadataDeserializer {
          {abstract} + deserialize()
      }
    }
  }
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageSerializer .up.|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer
@enduml

```

 - [ ] A l'aide du schéma ci-dessus, 
     - [ ] reconstituez la hérarchie d'**interfaces** et de **classes**.
     - [ ] déplacez si besoin l'existant en respect du nouveau **nommage de packages**
     - [ ] **Implémentez** les interfaces suivantes en laissant sans logique les implémentations pour commencer.
     - [ ] Comme toujours, gagnez des points en **commentant** le code (**Javadoc**)

#### Interfaces

##### Interface d'Accès direct à la base

```java
public interface DirectAccessDatabaseDeserializer extends DatabaseDeserializer<WalletFrameMedia> {
    void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException;
    <T extends OutputStream> T getSourceOutputStream();
    <T extends OutputStream> void setSourceOutputStream(T os);
}
```

##### Interface pour la désérialisation des Métadonnées

```java
public interface MetadataDeserializer {
   List<DigitalBadgeMetadata> deserialize(WalletFrameMedia media) throws IOException;
}
```

#### Metadonnées


```java
public class MetadataDeserializerDatabaseImpl implements MetadataDeserializer {
    
    @Override
    public List<DigitalBadgeMetadata> deserialize(WalletFrameMedia media) throws IOException {

        // A COMPLéTER
    }

    /**
     * A COMMENTER
     */
    public static String readLastLine(RandomAccessFile file) throws IOException {
        StringBuilder builder = new StringBuilder();
        long length = file.length();
        for(long seek = length; seek >= 0; --seek){
            file.seek(seek);
            char c = (char)file.read();
            if(c != '\n' || seek == 1)
            {
                builder.append(c);
            }
            else{
                builder = builder.reverse();
                break;
            }
        }
        return builder.toString();
    }

}
```

#### Sérialisation

```java
@Override
public final void serialize(File source, WalletFrameMedia media) throws IOException {
     long size = Files.size(source.toPath());
     try(OutputStream os = media.getEncodedImageOutput()) {
        long numberOfLines = media.getNumberOfLines();
        PrintWriter writer = new PrintWriter(os, true, StandardCharsets.UTF_8);
        writer.printf("%1$d;", size);
        try(OutputStream eos = getSerializingStream(media)) {
           getSourceInputStream(source).transferTo(eos);
           eos.flush();
        }
        writer.printf("\n");
        writer.printf("%1$d;%2$d;",numberOfLines + 2, media.getChannel().getFilePointer());
     }
     media.incrementLines();
}
```

#### Désérialisation


```java
@Override
public void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException {
     long pos = meta.getWalletPosition();
     media.getChannel().seek(pos);
     String[] data = media.getEncodedImageReader(false).readLine().split(";");
     try (OutputStream os = getSourceOutputStream()) {
        getDeserializingStream(data[3]).transferTo(os);
     }
}
```
 
 - [ ] **Opération compilation**: 
   - [ ] Veiller à ce que l'ensemble du projet compile, rectifier si nécessaire votre code (mais pas celui qui vous est imposé), 
   - [ ] Implémenter la logique là où le commentaire **"A COMPLéTER"** est présent.
 - [ ] **Opération validation**:
   - [ ] Itérer autant de fois que nécessaire pour rectifier le code afin de passer au vert
     - [ ] Le nouveau **test unitaire** dont le code est donné en fin d'énoncé. 
     - [ ] Mais aussi l'**ensemble des tests unitaires** déjà présents dans le projet (points bonus)
     - [ ] Quel que soit votre niveau de réussite, donnez un **aperçu** de votre performance avec une saisie d'écran ;), par exemple comme ceci:
       ![preuve][preuve]

----


## ANNEXE: Classe de Test Unitaire


```java
public class DirectAccessBadgeWalletDAOTest {

    private static final Logger LOG = LogManager.getLogger(DirectAccessBadgeWalletDAOTest.class);
    
    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.csv");
    
    @BeforeEach
    public void init() throws IOException {
        if (walletDatabase.exists()){
            walletDatabase.delete();
            walletDatabase.createNewFile();
        }
    }

    @Test
    public void testAddBadgeOnDatabase(){
    
    try {
    
        DirectAccessBadgeWalletDAO dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "wallet.csv");
        
        // 1er Badge
        File image = new File(RESOURCES_PATH + "petite_image.png");
        dao.addBadge(image);
        
        // 2ème Badge
        File image2 = new File(RESOURCES_PATH + "petite_image_2.png");
        dao.addBadge(image2);
        
        // 3ème Badge
        File image3 = new File(RESOURCES_PATH + "superman.jpg");
        dao.addBadge(image3);
        
        try (BufferedReader reader = new BufferedReader(new FileReader(walletDatabase))) {
        
           ImageSerializer serializer = new ImageSerializerBase64Impl();
        
           String serializedImage = reader.readLine();
           LOG.info("1ère ligne:\n{}", serializedImage);
           String encodedImage = (String) serializer.serialize(image);
           serializedImage = serializedImage.replaceAll("\n", "").replaceAll("\r", "");
           String[] data = serializedImage.split(";");
        
           String serializedImage2 = reader.readLine();
           LOG.info("2ème ligne:\n{}", serializedImage2);
           String encodedImage2 = (String) serializer.serialize(image2);
           serializedImage2 = serializedImage2.replaceAll("\n", "").replaceAll("\r", "");
           String[] data2 = serializedImage2.split(";");
        
           String serializedImage3 = reader.readLine();
           LOG.info("3ème ligne:\n{}", serializedImage3);
           String encodedImage3 = (String) serializer.serialize(image3);
           serializedImage3 = serializedImage3.replaceAll("\n", "").replaceAll("\r", "");
           String[] data3 = serializedImage3.split(";");
        
           // Assertions
        
           assertEquals(1, Integer.parseInt(data[0]));
           assertEquals(0, Integer.parseInt(data[1]));
           assertEquals(Files.size(image.toPath()), Long.parseLong(data[2]));
           assertEquals(encodedImage, data[3]);
        
           assertEquals(2, Integer.parseInt(data2[0]));
           assertEquals(753, Integer.parseInt(data2[1]));
           assertEquals(Files.size(image2.toPath()), Long.parseLong(data2[2]));
           assertEquals(encodedImage2, data2[3]);
        
           assertEquals(3, Integer.parseInt(data3[0]));
           assertEquals(1972, Integer.parseInt(data3[1]));
           assertEquals(Files.size(image3.toPath()), Long.parseLong(data3[2]));
           assertEquals(encodedImage3, data3[3]);
        }
        
        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }

    @Test
    public void testGetBadgeFromDatabase(){
        try {
            BadgeWalletDAO dao = new MultiBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.csv");
        
            File originImage1 = new File(RESOURCES_PATH + "petite_image.png");
            File originImage2 = new File(RESOURCES_PATH + "petite_image_2.png");
            File originImage3 = new File(RESOURCES_PATH + "superman.jpg");
            File extractedImage1 = new File(RESOURCES_PATH + "petite_image_ext.png");
            File extractedImage2 = new File(RESOURCES_PATH + "petite_image_2_ext.png");
            File extractedImage3 = new File(RESOURCES_PATH + "superman_ext.jpg");
            
            OutputStream fileBadgeStream1 = new FileOutputStream(extractedImage1);
            dao.getBadge(fileBadgeStream1);
            OutputStream fileBadgeStream2 = new FileOutputStream(extractedImage2);
            dao.getBadge(fileBadgeStream2);
            OutputStream fileBadgeStream3 = new FileOutputStream(extractedImage3);
            dao.getBadge(fileBadgeStream3);
            
            assertArrayEquals(Files.readAllBytes(originImage1.toPath()), Files.readAllBytes(extractedImage1.toPath()));
            LOG.info("Badge 1 récupéré avec succès");
            assertArrayEquals(Files.readAllBytes(originImage2.toPath()), Files.readAllBytes(extractedImage2.toPath()));
            LOG.info("Badge 2 récupéré avec succès");
            assertArrayEquals(Files.readAllBytes(originImage3.toPath()), Files.readAllBytes(extractedImage3.toPath()));
            LOG.info("Badge 3 récupéré avec succès");
        
        } catch (Exception e) {
            LOG.error("Test en échec ! ", e);
            fail();
        }
    }

    @Test
    void testGetMetadata() throws IOException {
        DirectAccessBadgeWalletDAO dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
        List<DigitalBadgeMetadata> metaList = dao.getWalletMetadata();
        LOG.info("Et voici les Métadonnées: {}", dao.getWalletMetadata());
        
        assertEquals(3, metaList.size());
        assertEquals(metaList.get(0), new DigitalBadgeMetadata(1, 0,557));
        assertEquals(metaList.get(1), new DigitalBadgeMetadata(2,753, 906));
        assertEquals(metaList.get(2), new DigitalBadgeMetadata(3,1972, 35664));
    }

  @Test
  public void testGetBadgeFromDatabaseByMetadata(){
    try {
        BadgeWalletDAO dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");

        File originImage1 = new File(RESOURCES_PATH + "petite_image.png");
        File originImage2 = new File(RESOURCES_PATH + "petite_image_2.png");
        File originImage3 = new File(RESOURCES_PATH + "superman.jpg");
        File extractedImage1 = new File(RESOURCES_PATH + "petite_image_ext.png");
        File extractedImage2 = new File(RESOURCES_PATH + "petite_image_2_ext.png");
        File extractedImage3 = new File(RESOURCES_PATH + "superman_ext.jpg");
        
        OutputStream fileBadgeStream1 = new FileOutputStream(extractedImage1);
        ((DirectAccessBadgeWalletDAOImpl) dao).getBadgeFromMetadata(fileBadgeStream1, new DigitalBadgeMetadata(1, 0,557));
        OutputStream fileBadgeStream2 = new FileOutputStream(extractedImage2);
        ((DirectAccessBadgeWalletDAOImpl) dao).getBadgeFromMetadata(fileBadgeStream2, new DigitalBadgeMetadata(2,753, 906));
        OutputStream fileBadgeStream3 = new FileOutputStream(extractedImage3);
        ((DirectAccessBadgeWalletDAOImpl) dao).getBadgeFromMetadata(fileBadgeStream3, new DigitalBadgeMetadata(3,1972, 35664));
        
        assertArrayEquals(Files.readAllBytes(originImage1.toPath()), Files.readAllBytes(extractedImage1.toPath()));
        LOG.info("Badge 1 récupéré avec succès");
        assertArrayEquals(Files.readAllBytes(originImage2.toPath()), Files.readAllBytes(extractedImage2.toPath()));
        LOG.info("Badge 2 récupéré avec succès");
        assertArrayEquals(Files.readAllBytes(originImage3.toPath()), Files.readAllBytes(extractedImage3.toPath()));
        LOG.info("Badge 3 récupéré avec succès");

    } catch (Exception e) {
        LOG.error("Test en échec ! ", e);
        fail();
    }
  }
}
```

[preuve]: screenshots/All-Tests.png "Echantillon principal"


----

### RESSOURCES

 * Pour vous aider, (il est possible d'avoir des problèmes d'encodage donc faisons un geste...), et malgré le fait que vous devriez penser à sa mise à jour, voici le csv échantillon pour la nouvelle version du test Unitaire, à placer dans les ressources de test:
  * [db_wallet_indexed.csv](badges-wallet/src/test/resources/db_wallet_indexed.csv), à télécharger au format brut.



